import { Component, OnInit } from '@angular/core';

import { IMAGES } from 'src/types/types';
import { ImagesService } from '../images.service';
import { ImageType } from 'src/types/interfaces';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  private readonly images = IMAGES;

  private sliderOptions = {
    initialSlide: 1,
    slidesPerView: Math.floor(window.innerWidth / 275),
    loop: true,
    centeredSlides: true,
    spaceBetween: 20,
    autoplay: true
  };

  private environmentImages: ImageType[];

  constructor(private imageService: ImagesService) {
    this.environmentImages = imageService.getRandomHomeEnvironmentImages();
  }

  ngOnInit() {
  }

}
