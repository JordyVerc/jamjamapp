import { Injectable } from '@angular/core';
import { ImageType } from 'src/types/interfaces';
import { shuffle } from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class ImagesService {

  private baseURL: string = 'assets/images/';
  private homeEnvirontImages: ImageType[];

  constructor() {
    this.homeEnvirontImages = [];
    for (let i = 1; i < 6; i++) {
      this.homeEnvirontImages.push({url: `${this.baseURL}home_environments/${i}.JPG`})
    }
  }

  public getRandomHomeEnvironmentImages(): ImageType[] {
    return shuffle(this.homeEnvirontImages);
  }

}
