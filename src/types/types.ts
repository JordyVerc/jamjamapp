const baseUrl = 'assets/images/';

export const IMAGES = {
    get HOME_BACKGROUND() { return `${baseUrl}home_background.JPG`; },
    get HOME_JAM_OF_LANDHUIS() { return `${baseUrl}home_jam_of_landhuys.png`; },
    get HOME_LANDHUIS_TRANSPARENT() { return `${baseUrl}home_landhuis_transparent.png`; },
    get HOME_WELCOME_TO_JAMJAM() { return `${baseUrl}home_welcome_to_jamjam.png`; }
}